using DataContext.Model;
using Microsoft.EntityFrameworkCore;

namespace DataContext.Data
{
    public class FilmExperienceContext : DbContext
    {
        public FilmExperienceContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<BlockReason> BlockReasons { get; set; }
        public DbSet<Filmcheque> Filmcheque { get; set; }
        public DbSet<Printer> Printer { get; set; }
        public DbSet<ProductType> ProductType { get; set; }
        public DbSet<Film> Film { get; set; }
        public DbSet<Workplace> Workplace { get; set; }
        public DbSet<Address> Address { get; set; }

        public DbSet<Product> Product { get; set; }
        public DbSet<Cinema> Cinema { get; set; }
        public DbSet<EmployeeRole> EmployeeRole { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Role> Role { get; set; }

        public DbSet<OnlineOrder> OnlineOrder { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Printer>()
                .HasKey(c => new {c.PrinterNumber});
            
            modelBuilder.Entity<BlockReason>()
                .HasKey(c => new {c.Reason});
            
            modelBuilder.Entity<Film>()
                .HasKey(c => new {c.FilmNumber});
            
            modelBuilder.Entity<Cinema>()
                .HasKey(c => new {c.CinemaNumber});

            modelBuilder.Entity<Workplace>()
                .HasKey(c => new {c.WorkplaceNumber});

            modelBuilder.Entity<Filmcheque>()
                .HasKey(c => new {c.FilmchequeNumber});

            modelBuilder.Entity<Filmcheque>()
                .HasMany(f => f.Products);

            modelBuilder.Entity<Product>()
                .HasKey(c => new {c.ProductNumber});

            modelBuilder.Entity<ProductType>()
                .HasKey(c => new {c.ProductTypeName});

            modelBuilder.Entity<Employee>()
                .HasKey(c => new {c.EmployeeNumber});


            modelBuilder.Entity<Role>()
                .HasKey(c => new {c.RoleName});

            modelBuilder.Entity<EmployeeRole>()
                .HasKey(t => new {t.EmployeeNumber, t.RoleName});

            modelBuilder.Entity<Employee>()
                .HasMany(pt => pt.EmployeeRoles)
                .WithOne(e => e.Employee)
                .HasForeignKey(pt => pt.EmployeeNumber);

            modelBuilder.Entity<Address>()
                .HasKey(c => new {c.AddressNumber});

            modelBuilder.Entity<OnlineOrder>()
                .HasKey(c => new {c.OrderNumber});

            modelBuilder.Entity<OnlineOrder>()
                .HasMany(f => f.Filmcheques);

        }
    }
}