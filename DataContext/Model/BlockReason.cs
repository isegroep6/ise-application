using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class BlockReason
    {
        public string Reason { get; set; }
    }
}