using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class OnlineOrder
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderNumber { get; set; }
        [ForeignKey("ShippingAddressNumber")]
        public Address ShippingAddress { get; set; }
        [ForeignKey("BillingAddressNumber")]
        public Address BillingAddress { get; set; }
        public string PersonalMessage { get; set; } 
        
        public List<Filmcheque> Filmcheques { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}