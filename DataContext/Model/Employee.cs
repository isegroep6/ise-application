using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Employee
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeNumber { get; set; }
        
        public string Email { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
        
        public List<EmployeeRole> EmployeeRoles { get; set; }
        public List<Workplace> Workplaces { get; set; }
    }
}