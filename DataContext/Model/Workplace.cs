using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Workplace
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkplaceNumber { get; set; }

        [ForeignKey("EmployeeNumber")]
        public Employee Employee { get; set; }
        [ForeignKey("CinemaNumber")]
        public Cinema Cinema { get; set; }
        public int CinemaNumber { get; set; }

        public int SalespointNumber { get; set; }
        public string WorkplaceName { get; set; }
    }
}