using System;

namespace DataContext.Model
{
    public class FilmchequeProduct
    {
        public int FilmchequeNumber { get; set; }
        public int CinemaNumber { get; set; }
        public int RoomNumber { get; set; }
        public int SalespointNumber { get; set; }
        public int ProductNumber { get; set; }
    }
}