using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Filmcheque
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FilmchequeNumber { get; set; }
        public int CinemaNumber { get; set; }
        
        public int SalespointNumber { get; set; }
        public int ?OrderNumber { get; set; }

        [ForeignKey("BlockedByEmployeeNumber")]
        public Employee BlockedByEmployee { get; set; }
        
        [ForeignKey("SaleWorkplaceNumber")]
        public Workplace SaleWorkplace { get; set; }
        public int SalePrinterNumber { get; set; }
        
        [ForeignKey("PrintWorkplaceNumber")]
        public Workplace PrintWorkplace { get; set; }
        public string PrintStatus { get; set; }
        public bool ValidMonday { get; set; }
        public bool ValidTuesday { get; set; }
        public bool ValidWednesday { get; set; }
        public bool ValidThursday { get; set; }
        public bool ValidFriday { get; set; }
        public bool ValidSaturday { get; set; }
        public bool ValidSunday { get; set; }
        public DateTime ValidUntil { get; set; }
        [ForeignKey("BlockReason")]
        public BlockReason BlockedReason { get; set; }
        public string Remark { get; set; }

        public List<Product> Products { get; set; }
    }
}