namespace DataContext.Model
{
    public class ProductType
    {
        public string ProductTypeName { get; set; }
        public int ?FilmNumber { get; set; }
    }
}