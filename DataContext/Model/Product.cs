using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductNumber { get; set; }
        public string ProductTypeName { get; set; }
        public int RoomNumber { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string BarCode { get; set; }
        
        public int FilmchequeNumber { get; set; }
    }
}