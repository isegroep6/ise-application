using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Printer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PrinterNumber { get; set; }
        public string IpAddress { get; set; }
        public string PrintCode { get; set; }
    }
}