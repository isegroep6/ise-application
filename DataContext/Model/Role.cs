using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Role
    {
        public string RoleName { get; set; }
        public List<EmployeeRole> EmployeeRoles { get; set; }
    }
}