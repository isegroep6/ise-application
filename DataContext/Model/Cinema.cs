using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Cinema
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CinemaNumber { get; set; }
        [ForeignKey("EmployeeNumber")]
        public Employee Employee { get; set; }
        public string CinemaName { get; set; }
    }
}