using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class EmployeeRole
    {
        public int EmployeeNumber { get; set; }
        public string RoleName { get; set; }
        
        public Employee Employee { get; set; }
        public Role Role { get; set; }
    }
}