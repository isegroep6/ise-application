# Total Film Experience
Application to demo database created for ISE project

## Setup
1. Open `Web` project
2. Copy appsettings.json to appsettings.Development.json
3. Change the connection string to your local sqlserver instance

### Database
1. Create database called `filmcheques`
2. setup tables using `ISE generate.sql`
3. insert basic data using `test_data.sql`

## Login
1. Start application
2. Login using `admin@admin.com` as email and `admin` as password