using System.Collections.Generic;

namespace Web.Models
{
    public class BlockFilmchequeRequestModel
    {
        public string BlockReason { get; set; }
        public string Remark { get; set; }
    }
}