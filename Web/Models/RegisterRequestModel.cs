using System.Collections.Generic;

namespace Web.Models
{
    public class RegisterRequestModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}