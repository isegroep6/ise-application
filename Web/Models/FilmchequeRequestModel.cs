using System.Collections.Generic;

namespace Web.Models
{
    public class FilmchequeRequestModel
    {
        public List<string> ProductTypes { get; set; } = new List<string>();
        public int PrinterNumber { get; set; }
    }
}