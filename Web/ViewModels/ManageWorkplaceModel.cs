using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class ManageWorkplaceModel
    {
        public Workplace Workplace { get; set; }
    }
}