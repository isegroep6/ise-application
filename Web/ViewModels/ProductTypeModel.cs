using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class ProductTypeModel
    {
        public List<ProductType> ProductTypes { get; set; }
    }
}