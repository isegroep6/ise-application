using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class ManageFilmchequeModel
    {
        public Filmcheque Filmcheque { get; set; }
        public List<Employee> Employees { get; set; } = new List<Employee>();
        public List<Workplace> Workplaces { get; set; } = new List<Workplace>();
        public List<BlockReason> BlockReasons { get; set; } = new List<BlockReason>();
    }
}