using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class AddressModel
    {
        public List<Address> Addresses { get; set; }
        public List<Address> OldAddresses { get; set; }
    }
}