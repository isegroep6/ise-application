using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class EmployeeModel
    {
        public List<Employee> Employees { get; set; }
    }
}