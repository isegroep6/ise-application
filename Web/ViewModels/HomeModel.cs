using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class HomeModel
    {
        public List<Filmcheque> Filmcheques { get; set; }
        public int FilmchequeCount { get; set; }
        public int EmployeeCount { get; set; }
        public int OrderCount { get; set; }
        public int ProductCount { get; set; }
        public int CinemaCount { get; set; }
    }
}