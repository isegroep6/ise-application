using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class ManageEmployeeModel
    {
        public Employee Employee { get; set; }
    }
}