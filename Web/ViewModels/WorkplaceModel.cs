using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class WorkplaceModel
    {
        public List<Workplace> Workplaces { get; set; }
    }
}