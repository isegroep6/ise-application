using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class OnlineOrderModel
    {
        public List<OnlineOrder> OnlineOrders { get; set; }
    }
}