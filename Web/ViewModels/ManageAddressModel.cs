using DataContext.Model;

namespace Web.ViewModels
{
    public class ManageAddressModel
    {
        public Address Address { get; set; }
    }
}