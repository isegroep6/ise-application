using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class FilmchequeModel
    {
        public List<Filmcheque> Filmcheques { get; set; }
        public List<ProductType> ProductTypes { get; set; }
        public List<Printer> Printers { get; set; }
    }
}