using System.Collections.Generic;
using DataContext.Model;

namespace Web.ViewModels
{
    public class ManageProductTypeModel
    {
        public ProductType ProductType { get; set; }
        public List<Film> Films { get; set; }
    }
}