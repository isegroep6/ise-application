using System.Linq;
using DataContext.Data;
using DataContext.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.ViewModels;
using Hasher = Microsoft.AspNet.Identity.PasswordHasher;

namespace Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EmployeeController : Controller
    {
        private readonly FilmExperienceContext _context;
        private readonly Hasher _hasher;

        public EmployeeController(FilmExperienceContext context)
        {
            _context = context;
            _hasher = new Hasher();
        }

        public IActionResult Index()
        {
            var model = new EmployeeModel
            {
                Employees = _context.Employee.ToList()
            };
            
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Edit(int employeeNumber)
        {
            var model = new ManageEmployeeModel
            {
                Employee = _context.Employee.Single(x => x.EmployeeNumber == employeeNumber)
            };
            
            return View(model);
        }
        
        public IActionResult Update(int employeeNumber, Employee employee)
        {
            var persistedEmployee = _context.Employee.Single(x => x.EmployeeNumber == employeeNumber);

            persistedEmployee.Email = employee.Email;
            
            if (!string.IsNullOrEmpty(employee.Password))
            {
                persistedEmployee.Password = _hasher.HashPassword(employee.Password);
            }

            _context.Update(persistedEmployee);
            _context.SaveChanges();
            
            return RedirectToAction("Index", "Employee");
        }
        
        public IActionResult Delete(int employeeNumber)
        {
            var employee = _context.Employee
                .Include(x => x.Workplaces)
                .Include(x => x.EmployeeRoles)
                .Single(x => x.EmployeeNumber == employeeNumber);

            foreach (var workplace in employee.Workplaces)
            {
                _context.Workplace.Remove(workplace);
            }
            
            foreach (var role in employee.EmployeeRoles)
            {
                _context.EmployeeRole.Remove(role);
            }
            _context.SaveChanges();

            _context.Remove(employee);
            _context.SaveChanges();
            
            return RedirectToAction("Index", "Employee");
        }
    }
}