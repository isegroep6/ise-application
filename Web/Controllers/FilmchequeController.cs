using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DataContext.Data;
using DataContext.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.ViewModels;
using Microsoft.AspNetCore.Identity;
using Web.Models;

namespace Web.Controllers
{
    [Authorize(Roles = "CreateFilmcheque")]
    public class FilmchequeController : Controller
    {
        private readonly FilmExperienceContext _context;
        private readonly UserManager<Employee> _userManager;

        public FilmchequeController(FilmExperienceContext context, UserManager<Employee> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            var model = new FilmchequeModel
            {
                Filmcheques = _context.Filmcheque
                    .Include(f => f.Products)
                    .Include(x => x.SaleWorkplace)
                    .ThenInclude(x => x.Cinema)
                    .ThenInclude(x => x.Employee)
                    .Include(f => f.PrintWorkplace)
                    .ThenInclude(x => x.Cinema)
                    .ThenInclude(x => x.Employee)
                    .Include(f => f.BlockedByEmployee)
                    .ToList(),
                ProductTypes = new List<ProductType>()
            };

            return View(model);
        }


        public IActionResult Create()
        {
            var model = new FilmchequeModel
            {
                ProductTypes = _context.ProductType.ToList(),
                Printers = _context.Printer.ToList()
            };

            return View(model);
        }

        public IActionResult Store(FilmchequeRequestModel model)
        {
            if (!model.ProductTypes.Any())
            {
                return RedirectToAction("Create", "Filmcheque");
            }
            
            var workplaceNumberFromAuthenticatedUser = HttpContext.Session.GetInt32(SessionModel.WorkplaceNumber);

            var workplace = _context.Workplace.Single(x => x.WorkplaceNumber == workplaceNumberFromAuthenticatedUser);

            var filmcheque = new Filmcheque
            {
                CinemaNumber = workplace.CinemaNumber,
                SalespointNumber = workplace.SalespointNumber,
                SalePrinterNumber = model.PrinterNumber,
                PrintWorkplace = workplace,
                SaleWorkplace = workplace,
                PrintStatus = "Sold",
                ValidMonday = true,
                ValidTuesday = true,
                ValidWednesday = true,
                ValidThursday = true,
                ValidFriday = true,
                ValidSaturday = true,
                ValidSunday = true,
                ValidUntil = DateTime.Now.AddYears(1),
                Products =  new List<Product>()
            };


            filmcheque.Products.AddRange(model.ProductTypes.Select(productType => new Product
            {
                ProductTypeName = productType,
                RoomNumber = 1,
                Price = 1,
                Amount = 1,
                BarCode = "1"
            }));

            _context.Filmcheque.Add(filmcheque);


            _context.SaveChanges();

            return RedirectToAction("Index", "Filmcheque");
        }

        [HttpGet]
        public IActionResult Show(int filmchequeNumber)
        {
            var model = new ManageFilmchequeModel
            {
                Filmcheque = _context.Filmcheque
                    .Include(f => f.Products)
                    .Include(f => f.SaleWorkplace)
                    .Include(f => f.PrintWorkplace)
                    .Single(x => x.FilmchequeNumber == filmchequeNumber),
                Employees = _context.Employee.ToList(),
                Workplaces = _context.Workplace
                    .Include(x => x.Cinema)
                    .Include(x => x.Employee)
                    .ToList(),
                BlockReasons = _context.BlockReasons.ToList()
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult Block(int filmchequeNumber)
        {
            var model = new ManageFilmchequeModel
            {
                Filmcheque = _context.Filmcheque.Single(x => x.FilmchequeNumber == filmchequeNumber),
                BlockReasons = _context.BlockReasons.ToList()
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult SetBlocked(int filmchequeNumber, BlockFilmchequeRequestModel model)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var filmcheque = _context.Filmcheque.Single(x => x.FilmchequeNumber == filmchequeNumber);
            filmcheque.PrintStatus = "blocked";
            filmcheque.BlockedByEmployee = user;
            filmcheque.BlockedReason = _context.BlockReasons.Single(x => x.Reason == model.BlockReason);
            filmcheque.Remark = model.Remark;

            try
            {
                _context.SaveChanges();

            }
            catch (DbUpdateException e)
            {
                ModelState.AddModelError(string.Empty, e.GetBaseException().Message);

                var dataModel = new ManageFilmchequeModel
                {
                    Filmcheque = _context.Filmcheque.Single(x => x.FilmchequeNumber == filmchequeNumber),
                    BlockReasons = _context.BlockReasons.ToList()
                };

                return View("Block", dataModel);
            }
            

            return RedirectToAction("Index", "Filmcheque");
        }
    }
}