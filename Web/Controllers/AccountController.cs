using System.Threading.Tasks;
using DataContext.Data;
using DataContext.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Web.Models;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Web.ViewModels;

namespace Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly FilmExperienceContext _context;
        private readonly UserManager<Employee> _userManager;
        private readonly SignInManager<Employee> _signInManager;

        public AccountController(FilmExperienceContext context, UserManager<Employee> userManager,
            SignInManager<Employee> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginRequestModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, true,
                false);
            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return View(model);
            }


            if (HttpContext.Session.GetInt32(SessionModel.WorkplaceNumber) == null)
            {
                return RedirectToAction("Workplaces", "Account");
            }

            if (returnUrl != null)
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpGet]
        public IActionResult AccessDenied(string returnUrl = null)
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public IActionResult Manage()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Login", "Account");
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterRequestModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if (!ModelState.IsValid) return View(model);

            var user = new Employee
            {
                Email = model.Email,
                Password = model.Password,
                Status = "Active"
            };

            await _userManager.CreateAsync(user);

            await _signInManager.SignInAsync(user, false);

            if (returnUrl != null)
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Workplaces()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var model = new WorkplaceModel
            {
                Workplaces = _context.Workplace.Where(x => x.Employee.EmployeeNumber == user.EmployeeNumber).ToList()
            };

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public IActionResult UseWorkplace(int workplaceId)
        {
            HttpContext.Session.SetInt32(SessionModel.WorkplaceNumber, workplaceId);


            return RedirectToAction("Index", "Home");
        }
    }
}