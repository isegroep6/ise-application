using System;
using System.Collections.Generic;
using System.Linq;
using DataContext.Data;
using DataContext.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.ViewModels;

namespace Web.Controllers
{
    [Authorize]
    public class AddressController : Controller
    {
        private readonly FilmExperienceContext _context;

        public AddressController(FilmExperienceContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var model = new AddressModel
            {
                Addresses = _context.Address.ToList(),
                OldAddresses = GetOldAddresses()
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new ManageAddressModel
            {
                Address = new Address()
            };

            return View("./Edit", model);
        }

        [HttpPost]
        public IActionResult Store(Address address)
        {
            _context.Address.Add(address);
            _context.SaveChanges();


            return RedirectToAction("Index", "Address");
        }


        [HttpGet]
        public IActionResult Edit(int addressNumber)
        {
            var model = new ManageAddressModel
            {
                Address = _context.Address.Single(x => x.AddressNumber == addressNumber)
            };

            return View(model);
        }

        public IActionResult Update(int addressNumber, Address address)
        {
            var persistedAddress = _context.Address.Single(x => x.AddressNumber == addressNumber);

            persistedAddress.City = address.City;
            persistedAddress.Email = address.Email;
            persistedAddress.Firstname = address.Firstname;
            persistedAddress.Lastname = address.Lastname;
            persistedAddress.Street = address.Street;
            persistedAddress.AddressNumber = address.AddressNumber;
            persistedAddress.CompanyName = address.CompanyName;
            persistedAddress.PostalCode = address.PostalCode;

            _context.Update(persistedAddress);
            _context.SaveChanges();

            return RedirectToAction("Index", "Address");
        }

        public IActionResult Delete(int addressNumber)
        {
            var productType = _context.Address.Single(x => x.AddressNumber == addressNumber);
            _context.Remove(productType);
            _context.SaveChanges();

            return RedirectToAction("Index", "Address");
        }

        // AnonymizeOldAddresses
        public IActionResult AnonymizeOldAddresses()
        {
            var oldAddresses = GetOldAddresses();

            foreach (var address in oldAddresses)
            {
                AnonymizeAddress(address);
            }
            
            _context.UpdateRange(oldAddresses);
            _context.SaveChanges();
            
            return RedirectToAction("Index", "Address");
        }

        private void AnonymizeAddress(Address address)
        {
            var obfuscationToken = "xxxx";

            address.City = obfuscationToken;
            address.Email = obfuscationToken;
            address.Firstname = obfuscationToken;
            address.Lastname = obfuscationToken;
            address.Street = obfuscationToken;
            address.CompanyName = obfuscationToken;
        }
        
        private List<Address> GetOldAddresses()
        {
            var addressIdsUsedByOrdersFromLastTwoYears = _context.OnlineOrder
                .Include(x => x.BillingAddress)
                .Include(x => x.ShippingAddress)
                .Where(o => o.CreatedAt >= DateTime.Now.AddYears(-2))
                .SelectMany(o => new List<int> {o.BillingAddress.AddressNumber, o.ShippingAddress.AddressNumber})
                .ToList();

            return _context.Address
                .Where(x => !addressIdsUsedByOrdersFromLastTwoYears.Contains(x.AddressNumber))
                .ToList();
        }
    }
}