using System.Linq;
using DataContext.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.ViewModels;

namespace Web.Controllers
{
    public class OnlineOrderController : Controller
    {
        private readonly FilmExperienceContext _context;

        public OnlineOrderController(FilmExperienceContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var model = new OnlineOrderModel
            {
                OnlineOrders = _context.OnlineOrder
                    .Include(x => x.BillingAddress)
                    .Include(x => x.ShippingAddress)
                    .Include(x => x.Filmcheques)
                    .ThenInclude(f => f.Products)
                    .ToList()
            };
            return View(model);
        }
    }
}