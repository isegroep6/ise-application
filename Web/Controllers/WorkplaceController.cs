using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DataContext.Data;
using DataContext.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Web.Authentication;
using Web.Models;
using Web.ViewModels;

namespace Web.Controllers
{
    [Authorize]
    public class WorkplaceController : Controller
    {
        private readonly FilmExperienceContext _context;
        private readonly Microsoft.AspNetCore.Identity.UserManager<Employee> _userManager;

        public WorkplaceController(FilmExperienceContext context,
            Microsoft.AspNetCore.Identity.UserManager<Employee> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var model = new WorkplaceModel
            {
                Workplaces = _context.Workplace.Where(x => x.Employee.EmployeeNumber == user.EmployeeNumber).ToList()
            };

            return View(model);
        }

         
        [HttpGet]
        public IActionResult Edit(int employeeNumber)
        {
            var model = new ManageEmployeeModel
            {
                Employee = _context.Employee.Single(x => x.EmployeeNumber == employeeNumber)
            };
            
            return View(model);
        }
        
        public IActionResult Update(int employeeNumber, Employee employee)
        {
            var persistedEmployee = _context.Employee.Single(x => x.EmployeeNumber == employeeNumber);

            persistedEmployee.Email = employee.Email;

            _context.Update(persistedEmployee);

            return RedirectToAction("Index", "Employee");
        }

        public IActionResult Delete()
        {
            var model = new EmployeeModel
            {
                Employees = _context.Employee.ToList()
            };

            return RedirectToAction("Index", "Employee");
        }
    }
}