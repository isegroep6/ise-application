﻿using System.Diagnostics;
using System.Linq;
using DataContext.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.Models;
using Web.ViewModels;

namespace Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly FilmExperienceContext _context;

        public HomeController(FilmExperienceContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var model = new HomeModel
            {
                Filmcheques = _context.Filmcheque.Include(x => x.Products).Take(10).ToList(),
                EmployeeCount = _context.Employee.Count(),
                FilmchequeCount = _context.Filmcheque.Count(),
                OrderCount = _context.OnlineOrder.Count(),
                ProductCount = _context.Product.Count(),
                CinemaCount = _context.Cinema.Count()
            };
            
            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}