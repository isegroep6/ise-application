using System.Linq;
using DataContext.Data;
using DataContext.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.ViewModels;

namespace Web.Controllers
{
    [Authorize]
    public class ProductTypeController : Controller
    {
        private readonly FilmExperienceContext _context;

        public ProductTypeController(FilmExperienceContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var model = new ProductTypeModel
            {
                ProductTypes = _context.ProductType.ToList()
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new ManageProductTypeModel
            {
                ProductType = new ProductType(),
                Films = _context.Film.ToList()
            };

            return View("./Edit", model);
        }

        [HttpPost]
        public IActionResult Store(ProductType productType)
        {
            _context.ProductType.Add(productType);
            _context.SaveChanges();


            return RedirectToAction("Index", "ProductType");
        }


        [HttpGet]
        public IActionResult Edit(string productTypeName)
        {
            var model = new ManageProductTypeModel
            {
                ProductType = _context.ProductType.Single(x => x.ProductTypeName == productTypeName),
                Films = _context.Film.ToList()
            };

            return View(model);
        }

        public IActionResult Update(string productTypeName, ProductType productType)
        {
            var persistedProductType = _context.ProductType.Single(x => x.ProductTypeName == productTypeName);

            persistedProductType.ProductTypeName = productType.ProductTypeName;
            persistedProductType.FilmNumber = productType.FilmNumber;

            _context.Update(persistedProductType);
            _context.SaveChanges();

            return RedirectToAction("Index", "ProductType");
        }

        public IActionResult Delete(string productTypeName)
        {
            var productType = _context.ProductType.Single(x => x.ProductTypeName == productTypeName);
            _context.Remove(productType);
            _context.SaveChanges();

            return RedirectToAction("Index", "ProductType");
        }
    }
}