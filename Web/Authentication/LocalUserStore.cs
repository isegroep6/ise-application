using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DataContext.Data;
using DataContext.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Hasher = Microsoft.AspNet.Identity.PasswordHasher;


namespace Web.Authentication
{
    public class LocalUserStore :
        IUserStore<Employee>,
        IUserClaimStore<Employee>,
        IUserLoginStore<Employee>,
        IUserRoleStore<Employee>,
        IUserPasswordStore<Employee>,
        IUserSecurityStampStore<Employee>
    {
        private readonly FilmExperienceContext _context;
        private readonly Hasher _hasher;

        public LocalUserStore(FilmExperienceContext context)
        {
            _context = context;
            _hasher = new Hasher();
        }

        public void Dispose()
        {
        }

        public Task<string> GetUserIdAsync(Employee user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.EmployeeNumber + "");
        }

        public Task<string> GetUserNameAsync(Employee user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        public Task SetUserNameAsync(Employee user, string userName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<string> GetNormalizedUserNameAsync(Employee user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task SetNormalizedUserNameAsync(Employee user, string normalizedName,
            CancellationToken cancellationToken)
        {
            user.Email = normalizedName;

            return Task.CompletedTask;
        }

        public Task<IdentityResult> CreateAsync(Employee user, CancellationToken cancellationToken)
        {
            user.Password = _hasher.HashPassword(user.Password);
            _context.Employee.Add(user);

            _context.SaveChanges();

            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> UpdateAsync(Employee user, CancellationToken cancellationToken)
        {
            var employee = _context.Employee.First(x => x.EmployeeNumber == user.EmployeeNumber);
            employee.Email = employee.Email;
            employee.Password = employee.Password;
            employee.Status = employee.Status;

            _context.Employee.Update(employee);
            
            _context.SaveChanges();
            
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> DeleteAsync(Employee user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<Employee> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return _context.Employee.FirstOrDefaultAsync(e => e.EmployeeNumber.ToString() == userId, cancellationToken);
        }

        public Task<Employee> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return Task.FromResult(_context.Employee.FirstOrDefault(e => e.Email == normalizedUserName));
        }

        public Task<IList<Claim>> GetClaimsAsync(Employee user, CancellationToken cancellationToken)
        {
            return Task.FromResult<IList<Claim>>(new List<Claim>());
        }

        public Task AddClaimsAsync(Employee user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task ReplaceClaimAsync(Employee user, Claim claim, Claim newClaim, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task RemoveClaimsAsync(Employee user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IList<Employee>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task AddLoginAsync(Employee user, UserLoginInfo login, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task RemoveLoginAsync(Employee user, string loginProvider, string providerKey,
            CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(Employee user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<Employee> FindByLoginAsync(string loginProvider, string providerKey,
            CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task AddToRoleAsync(Employee user, string roleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task RemoveFromRoleAsync(Employee user, string roleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(Employee user, CancellationToken cancellationToken)
        {
            return Task.FromResult((IList<string>) _context.EmployeeRole
                .Where(x => x.EmployeeNumber == user.EmployeeNumber)
                .Select(x => x.RoleName)
                .ToList());
        }

        public Task<bool> IsInRoleAsync(Employee user, string roleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IList<Employee>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task SetPasswordHashAsync(Employee user, string passwordHash, CancellationToken cancellationToken)
        {
            user.Password = passwordHash;

            return Task.CompletedTask;
        }

        public Task<string> GetPasswordHashAsync(Employee user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Password);
        }

        public Task<bool> HasPasswordAsync(Employee user, CancellationToken cancellationToken)
        {
            return Task.FromResult(!string.IsNullOrEmpty(user.Password));
        }

        public Task SetSecurityStampAsync(Employee user, string stamp, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task<string> GetSecurityStampAsync(Employee user, CancellationToken cancellationToken)
        {
            return Task.FromResult("test");
        }
    }
}